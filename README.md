# Mezzanine Calendar JavaScript utils
Useful JS routines / helpers for Mezzanine Calendar.

<a name="payWithStripe"></a>

## payWithStripe(form, config)
Gathers payment info using Stripe's `checkout.js` and submits the generated token to the
server via a form. The `working` class will be added to the form while the pop-up is open.

**YOU MUST INCLUDE `checkout.js` BEFORE CALLING THIS FUNCTION**

For this DOM structure:

```html
<form id="my-form" method="POST">
 <input type="hidden" id="id_stripe_token" />
 <input type="number" id="id_donation" value="5" />

 <input type="number" class="ticket-quantity" data-cost="100" value="3" />
 <input type="number" class="ticket-quantity" data-cost="200" value="2" />
</form>

<script src="https://checkout.stripe.com/checkout.js"></script>
<script>
 payWithStripe(document.querySelector('#my-form'), {
 	key: 'YOUR KEY',
 	name: 'YOUR SITE NAME',
 	// Pass more checkout.js options here
 });
</script>
```

The user will be shown the pop-up with a charge of:

- Donation: 5 (will be converted to 500 cents)
- Ticket A: 3 tickets, 100 each (already in cents)
- Ticket B: 2 tickets, 200 each (already in cents)
- Total in cents: 500 + 300 + 400 = 1200

**Kind**: global function
**See**: [checkout.js docs](https://stripe.com/docs/checkout#integration-custom)

| Param | Type | Description |
| --- | --- | --- |
| form | <code>HTMLFormElement</code> | Will be submitted after payment details are captured. This form must also contain all inputs listed in the properties section (see example above). |
| config | <code>Object</code> | Configuration object for `checkout.js`. The only parameter you absolutely have to pass is `key`. The `amount` and `token` will be created automatically. You can then pass any optional parameters to customize the checkout experience. |

**Properties**

| Name | Type | Description |
| --- | --- | --- |
| tokenInput | <code>HTMLInputElement</code> | An input with `id="id_stripe_token"` to receive the generated token. |
| ticketInputs | <code>Array.&lt;HTMLInputElement&gt;</code> | One or more inputs with `class="ticket-quantity"`with a `data-cost` attribute representing individual ticket prices (in cents). The value is the amount of tickets the user will buy. |
| donationInput | <code>HTMLInputElement</code> | Optional input with `id="id_donation"` to indicate a donation amount (will be converted to cents). |

<a name="initMap"></a>

## initMap(canvas, address)
Initialize Google Maps with a marker in place.

**YOU MUST INCLUDE GOOGLE'S MAP API BEFORE CALLING THIS FUNCTION**

```html
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
  var canvas = document.querySelector('#map-canvas');
  var address = '1600 Pennsylvania Avenue, Washington DC';
  initMap(canvas, address);
</script>
```

**Kind**: global function

| Param | Type | Description |
| --- | --- | --- |
| canvas | <code>HTMLElement</code> | The HTML element that will receive the map |
| address | <code>String</code> | The address where the map marker will be placed |

