(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["mezzanineCalendarUtils"] = factory();
	else
		root["mezzanineCalendarUtils"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["payWithStripe"] = payWithStripe;
/* harmony export (immutable) */ __webpack_exports__["initMap"] = initMap;
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

/* globals StripeCheckout google */
/* eslint-disable no-alert */

/**
 * Gathers payment info using Stripe's `checkout.js` and submits the generated token to the
 * server via a form. The `working` class will be added to the form while the pop-up is open.
 *
 * **YOU MUST INCLUDE `checkout.js` BEFORE CALLING THIS FUNCTION**
 *
 * For this DOM structure:
 *
 * ```html
 * <form id="my-form" method="POST">
 *  <input type="hidden" id="id_stripe_token" />
 *  <input type="number" id="id_donation" value="5" />
 *
 *  <input type="number" class="ticket-quantity" data-cost="100" value="3" />
 *  <input type="number" class="ticket-quantity" data-cost="200" value="2" />
 * </form>
 *
 * <script src="https://checkout.stripe.com/checkout.js"></script>
 * <script>
 *  payWithStripe(document.querySelector('#my-form'), {
 *  	key: 'YOUR KEY',
 *  	name: 'YOUR SITE NAME',
 *  	// Pass more checkout.js options here
 *  });
 * </script>
 * ```
 *
 * The user will be shown the pop-up with a charge of:
 *
 * - Donation: 5 (will be converted to 500 cents)
 * - Ticket A: 3 tickets, 100 each (already in cents)
 * - Ticket B: 2 tickets, 200 each (already in cents)
 * - Total in cents: 500 + 300 + 400 = 1200
 *
 * @param {HTMLFormElement} form - Will be submitted after payment details are captured. This
 * form must also contain all inputs listed in the properties section (see example above).
 * @param {Object} config - Configuration object for `checkout.js`. The only parameter you
 * absolutely have to pass is `key`. The `amount` and `token` will be created automatically.
 * You can then pass any optional parameters to customize the checkout experience.

 * @property {HTMLInputElement} tokenInput - An input with `id="id_stripe_token"` to receive
 * the generated token.
 * @property {HTMLInputElement[]} ticketInputs - One or more inputs with
 * `class="ticket-quantity"`with a `data-cost` attribute representing individual ticket
 * prices (in cents). The value is the amount of tickets the user will buy.
 * @property {HTMLInputElement} [donationInput] - Optional input with `id="id_donation"`
 * to indicate a donation amount (will be converted to cents).
 *
 * @see  {@link https://stripe.com/docs/checkout#integration-custom|checkout.js docs}
 */
function payWithStripe(form, config) {
	var tokenInput = form.querySelector('#id_stripe_token');
	var donationInput = form.querySelector('#id_donation');
	var ticketInputs = form.querySelectorAll('.ticket-quantity');

	var total = 0; // The total must be passed as cents to Stripe
	var donation = 0;
	var paymentComplete = false;

	// We can't do anything without StripeCheckout
	if (typeof StripeCheckout === 'undefined') {
		alert('Error: Could not connect to our payment processor.');
		return;
	}

	// Indicate the form is being processed
	form.classList.add('working');

	// If the token input is already filled up, just submit the form
	if (tokenInput.value) {
		form.submit();
		return;
	}

	// Else, calculate the total based on tickets and donation
	[].concat(_toConsumableArray(ticketInputs)).forEach(function (ticketInput) {
		total += ticketInput.dataset.cost * ticketInput.value;
	});
	if (donationInput) {
		donation = parseFloat(donationInput.value).toFixed(2);
		donation = Math.round(donation * 100); // Convert to cents
		total += donation;
	}

	// Configure and display the checkout pop-up
	StripeCheckout.open(_extends({}, config, {
		amount: total,

		// Executed after the user fills the payment details
		token: function onTokenReady(token) {
			tokenInput.value = token.id;
			paymentComplete = true;
			form.submit();
		},

		// Executed whenever the payment dialog is closed
		// either after token() is called, or the user closes it
		closed: function cleanUp() {
			// If the user closed the pop-up...
			if (!paymentComplete) {
				form.classList.remove('working');
			}
		}
	}));
}

/**
 * Initialize Google Maps with a marker in place.
 *
 * **YOU MUST INCLUDE GOOGLE'S MAP API BEFORE CALLING THIS FUNCTION**
 *
 * ```html
 * <script src="https://maps.googleapis.com/maps/api/js"></script>
 * <script>
 *   var canvas = document.querySelector('#map-canvas');
 *   var address = '1600 Pennsylvania Avenue, Washington DC';
 *   initMap(canvas, address);
 * </script>
 * ```
 *
 * @param  {HTMLElement} canvas  The HTML element that will receive the map
 * @param  {String} address The address where the map marker will be placed
 */
function initMap(canvas, address) {
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({ address: address }, function (results, status) {
		if (status !== google.maps.GeocoderStatus.OK) {
			alert('Geocode was not successful for the following reason: ' + status);
			return;
		}
		var map = new google.maps.Map(canvas, {
			zoom: 16,
			center: results[0].geometry.location
		});
		var marker = new google.maps.Marker({
			map: map,
			position: results[0].geometry.location
		});
		var infoWindow = new google.maps.InfoWindow({
			content: address
		});
		google.maps.event.addListener(marker, 'click', function () {
			infoWindow.open(map, marker);
		});
	});
}

/***/ })
/******/ ]);
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCAzZGU1NWZhMWZhMzY1N2M4MjczOCIsIndlYnBhY2s6Ly8vLi9zcmMvaW5kZXguanMiXSwibmFtZXMiOlsicGF5V2l0aFN0cmlwZSIsImZvcm0iLCJjb25maWciLCJ0b2tlbklucHV0IiwicXVlcnlTZWxlY3RvciIsImRvbmF0aW9uSW5wdXQiLCJ0aWNrZXRJbnB1dHMiLCJxdWVyeVNlbGVjdG9yQWxsIiwidG90YWwiLCJkb25hdGlvbiIsInBheW1lbnRDb21wbGV0ZSIsIlN0cmlwZUNoZWNrb3V0IiwiYWxlcnQiLCJjbGFzc0xpc3QiLCJhZGQiLCJ2YWx1ZSIsInN1Ym1pdCIsImZvckVhY2giLCJ0aWNrZXRJbnB1dCIsImRhdGFzZXQiLCJjb3N0IiwicGFyc2VGbG9hdCIsInRvRml4ZWQiLCJNYXRoIiwicm91bmQiLCJvcGVuIiwiYW1vdW50IiwidG9rZW4iLCJvblRva2VuUmVhZHkiLCJpZCIsImNsb3NlZCIsImNsZWFuVXAiLCJyZW1vdmUiLCJpbml0TWFwIiwiY2FudmFzIiwiYWRkcmVzcyIsImdlb2NvZGVyIiwiZ29vZ2xlIiwibWFwcyIsIkdlb2NvZGVyIiwiZ2VvY29kZSIsInJlc3VsdHMiLCJzdGF0dXMiLCJHZW9jb2RlclN0YXR1cyIsIk9LIiwibWFwIiwiTWFwIiwiem9vbSIsImNlbnRlciIsImdlb21ldHJ5IiwibG9jYXRpb24iLCJtYXJrZXIiLCJNYXJrZXIiLCJwb3NpdGlvbiIsImluZm9XaW5kb3ciLCJJbmZvV2luZG93IiwiY29udGVudCIsImV2ZW50IiwiYWRkTGlzdGVuZXIiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxPO0FDVkE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsbURBQTJDLGNBQWM7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsbUNBQTJCLDBCQUEwQixFQUFFO0FBQ3ZELHlDQUFpQyxlQUFlO0FBQ2hEO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDhEQUFzRCwrREFBK0Q7O0FBRXJIO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7O0FDaEVBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0RPLFNBQVNBLGFBQVQsQ0FBdUJDLElBQXZCLEVBQTZCQyxNQUE3QixFQUFxQztBQUMzQyxLQUFNQyxhQUFhRixLQUFLRyxhQUFMLENBQW1CLGtCQUFuQixDQUFuQjtBQUNBLEtBQU1DLGdCQUFnQkosS0FBS0csYUFBTCxDQUFtQixjQUFuQixDQUF0QjtBQUNBLEtBQU1FLGVBQWVMLEtBQUtNLGdCQUFMLENBQXNCLGtCQUF0QixDQUFyQjs7QUFFQSxLQUFJQyxRQUFRLENBQVosQ0FMMkMsQ0FLNUI7QUFDZixLQUFJQyxXQUFXLENBQWY7QUFDQSxLQUFJQyxrQkFBa0IsS0FBdEI7O0FBRUE7QUFDQSxLQUFJLE9BQU9DLGNBQVAsS0FBMEIsV0FBOUIsRUFBMkM7QUFDMUNDLFFBQU0sb0RBQU47QUFDQTtBQUNBOztBQUVEO0FBQ0FYLE1BQUtZLFNBQUwsQ0FBZUMsR0FBZixDQUFtQixTQUFuQjs7QUFFQTtBQUNBLEtBQUlYLFdBQVdZLEtBQWYsRUFBc0I7QUFDckJkLE9BQUtlLE1BQUw7QUFDQTtBQUNBOztBQUVEO0FBQ0EsOEJBQUlWLFlBQUosR0FBa0JXLE9BQWxCLENBQTBCLHVCQUFlO0FBQ3hDVCxXQUFVVSxZQUFZQyxPQUFaLENBQW9CQyxJQUFwQixHQUEyQkYsWUFBWUgsS0FBakQ7QUFDQSxFQUZEO0FBR0EsS0FBSVYsYUFBSixFQUFtQjtBQUNsQkksYUFBV1ksV0FBV2hCLGNBQWNVLEtBQXpCLEVBQWdDTyxPQUFoQyxDQUF3QyxDQUF4QyxDQUFYO0FBQ0FiLGFBQVdjLEtBQUtDLEtBQUwsQ0FBV2YsV0FBVyxHQUF0QixDQUFYLENBRmtCLENBRXFCO0FBQ3ZDRCxXQUFTQyxRQUFUO0FBQ0E7O0FBRUQ7QUFDQUUsZ0JBQWVjLElBQWYsY0FDSXZCLE1BREo7QUFFQ3dCLFVBQVFsQixLQUZUOztBQUlDO0FBQ0FtQixTQUFPLFNBQVNDLFlBQVQsQ0FBc0JELEtBQXRCLEVBQTZCO0FBQ25DeEIsY0FBV1ksS0FBWCxHQUFtQlksTUFBTUUsRUFBekI7QUFDQW5CLHFCQUFrQixJQUFsQjtBQUNBVCxRQUFLZSxNQUFMO0FBQ0EsR0FURjs7QUFXQztBQUNBO0FBQ0FjLFVBQVEsU0FBU0MsT0FBVCxHQUFtQjtBQUMxQjtBQUNBLE9BQUksQ0FBQ3JCLGVBQUwsRUFBc0I7QUFDckJULFNBQUtZLFNBQUwsQ0FBZW1CLE1BQWYsQ0FBc0IsU0FBdEI7QUFDQTtBQUNEO0FBbEJGO0FBb0JBOztBQUVEOzs7Ozs7Ozs7Ozs7Ozs7OztBQWlCTyxTQUFTQyxPQUFULENBQWlCQyxNQUFqQixFQUF5QkMsT0FBekIsRUFBa0M7QUFDeEMsS0FBTUMsV0FBVyxJQUFJQyxPQUFPQyxJQUFQLENBQVlDLFFBQWhCLEVBQWpCO0FBQ0FILFVBQVNJLE9BQVQsQ0FBaUIsRUFBRUwsZ0JBQUYsRUFBakIsRUFBOEIsVUFBQ00sT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ2xELE1BQUlBLFdBQVdMLE9BQU9DLElBQVAsQ0FBWUssY0FBWixDQUEyQkMsRUFBMUMsRUFBOEM7QUFDN0NoQyxtRUFBOEQ4QixNQUE5RDtBQUNBO0FBQ0E7QUFDRCxNQUFNRyxNQUFNLElBQUlSLE9BQU9DLElBQVAsQ0FBWVEsR0FBaEIsQ0FBb0JaLE1BQXBCLEVBQTRCO0FBQ3ZDYSxTQUFNLEVBRGlDO0FBRXZDQyxXQUFRUCxRQUFRLENBQVIsRUFBV1EsUUFBWCxDQUFvQkM7QUFGVyxHQUE1QixDQUFaO0FBSUEsTUFBTUMsU0FBUyxJQUFJZCxPQUFPQyxJQUFQLENBQVljLE1BQWhCLENBQXVCO0FBQ3JDUCxXQURxQztBQUVyQ1EsYUFBVVosUUFBUSxDQUFSLEVBQVdRLFFBQVgsQ0FBb0JDO0FBRk8sR0FBdkIsQ0FBZjtBQUlBLE1BQU1JLGFBQWEsSUFBSWpCLE9BQU9DLElBQVAsQ0FBWWlCLFVBQWhCLENBQTJCO0FBQzdDQyxZQUFTckI7QUFEb0MsR0FBM0IsQ0FBbkI7QUFHQUUsU0FBT0MsSUFBUCxDQUFZbUIsS0FBWixDQUFrQkMsV0FBbEIsQ0FBOEJQLE1BQTlCLEVBQXNDLE9BQXRDLEVBQStDLFlBQU07QUFDcERHLGNBQVc3QixJQUFYLENBQWdCb0IsR0FBaEIsRUFBcUJNLE1BQXJCO0FBQ0EsR0FGRDtBQUdBLEVBbkJEO0FBb0JBLEMiLCJmaWxlIjoibWV6emFuaW5lLWNhbGVuZGFyLXV0aWxzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIHdlYnBhY2tVbml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uKHJvb3QsIGZhY3RvcnkpIHtcblx0aWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgPT09ICdvYmplY3QnKVxuXHRcdG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeSgpO1xuXHRlbHNlIGlmKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZClcblx0XHRkZWZpbmUoW10sIGZhY3RvcnkpO1xuXHRlbHNlIGlmKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0Jylcblx0XHRleHBvcnRzW1wibWV6emFuaW5lQ2FsZW5kYXJVdGlsc1wiXSA9IGZhY3RvcnkoKTtcblx0ZWxzZVxuXHRcdHJvb3RbXCJtZXp6YW5pbmVDYWxlbmRhclV0aWxzXCJdID0gZmFjdG9yeSgpO1xufSkodGhpcywgZnVuY3Rpb24oKSB7XG5yZXR1cm4gXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svdW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbiIsIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKVxuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuXG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBpZGVudGl0eSBmdW5jdGlvbiBmb3IgY2FsbGluZyBoYXJtb255IGltcG9ydHMgd2l0aCB0aGUgY29ycmVjdCBjb250ZXh0XG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmkgPSBmdW5jdGlvbih2YWx1ZSkgeyByZXR1cm4gdmFsdWU7IH07XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwge1xuIFx0XHRcdFx0Y29uZmlndXJhYmxlOiBmYWxzZSxcbiBcdFx0XHRcdGVudW1lcmFibGU6IHRydWUsXG4gXHRcdFx0XHRnZXQ6IGdldHRlclxuIFx0XHRcdH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDApO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIDNkZTU1ZmExZmEzNjU3YzgyNzM4IiwiLyogZ2xvYmFscyBTdHJpcGVDaGVja291dCBnb29nbGUgKi9cbi8qIGVzbGludC1kaXNhYmxlIG5vLWFsZXJ0ICovXG5cbi8qKlxuICogR2F0aGVycyBwYXltZW50IGluZm8gdXNpbmcgU3RyaXBlJ3MgYGNoZWNrb3V0LmpzYCBhbmQgc3VibWl0cyB0aGUgZ2VuZXJhdGVkIHRva2VuIHRvIHRoZVxuICogc2VydmVyIHZpYSBhIGZvcm0uIFRoZSBgd29ya2luZ2AgY2xhc3Mgd2lsbCBiZSBhZGRlZCB0byB0aGUgZm9ybSB3aGlsZSB0aGUgcG9wLXVwIGlzIG9wZW4uXG4gKlxuICogKipZT1UgTVVTVCBJTkNMVURFIGBjaGVja291dC5qc2AgQkVGT1JFIENBTExJTkcgVEhJUyBGVU5DVElPTioqXG4gKlxuICogRm9yIHRoaXMgRE9NIHN0cnVjdHVyZTpcbiAqXG4gKiBgYGBodG1sXG4gKiA8Zm9ybSBpZD1cIm15LWZvcm1cIiBtZXRob2Q9XCJQT1NUXCI+XG4gKiAgPGlucHV0IHR5cGU9XCJoaWRkZW5cIiBpZD1cImlkX3N0cmlwZV90b2tlblwiIC8+XG4gKiAgPGlucHV0IHR5cGU9XCJudW1iZXJcIiBpZD1cImlkX2RvbmF0aW9uXCIgdmFsdWU9XCI1XCIgLz5cbiAqXG4gKiAgPGlucHV0IHR5cGU9XCJudW1iZXJcIiBjbGFzcz1cInRpY2tldC1xdWFudGl0eVwiIGRhdGEtY29zdD1cIjEwMFwiIHZhbHVlPVwiM1wiIC8+XG4gKiAgPGlucHV0IHR5cGU9XCJudW1iZXJcIiBjbGFzcz1cInRpY2tldC1xdWFudGl0eVwiIGRhdGEtY29zdD1cIjIwMFwiIHZhbHVlPVwiMlwiIC8+XG4gKiA8L2Zvcm0+XG4gKlxuICogPHNjcmlwdCBzcmM9XCJodHRwczovL2NoZWNrb3V0LnN0cmlwZS5jb20vY2hlY2tvdXQuanNcIj48L3NjcmlwdD5cbiAqIDxzY3JpcHQ+XG4gKiAgcGF5V2l0aFN0cmlwZShkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjbXktZm9ybScpLCB7XG4gKiAgXHRrZXk6ICdZT1VSIEtFWScsXG4gKiAgXHRuYW1lOiAnWU9VUiBTSVRFIE5BTUUnLFxuICogIFx0Ly8gUGFzcyBtb3JlIGNoZWNrb3V0LmpzIG9wdGlvbnMgaGVyZVxuICogIH0pO1xuICogPC9zY3JpcHQ+XG4gKiBgYGBcbiAqXG4gKiBUaGUgdXNlciB3aWxsIGJlIHNob3duIHRoZSBwb3AtdXAgd2l0aCBhIGNoYXJnZSBvZjpcbiAqXG4gKiAtIERvbmF0aW9uOiA1ICh3aWxsIGJlIGNvbnZlcnRlZCB0byA1MDAgY2VudHMpXG4gKiAtIFRpY2tldCBBOiAzIHRpY2tldHMsIDEwMCBlYWNoIChhbHJlYWR5IGluIGNlbnRzKVxuICogLSBUaWNrZXQgQjogMiB0aWNrZXRzLCAyMDAgZWFjaCAoYWxyZWFkeSBpbiBjZW50cylcbiAqIC0gVG90YWwgaW4gY2VudHM6IDUwMCArIDMwMCArIDQwMCA9IDEyMDBcbiAqXG4gKiBAcGFyYW0ge0hUTUxGb3JtRWxlbWVudH0gZm9ybSAtIFdpbGwgYmUgc3VibWl0dGVkIGFmdGVyIHBheW1lbnQgZGV0YWlscyBhcmUgY2FwdHVyZWQuIFRoaXNcbiAqIGZvcm0gbXVzdCBhbHNvIGNvbnRhaW4gYWxsIGlucHV0cyBsaXN0ZWQgaW4gdGhlIHByb3BlcnRpZXMgc2VjdGlvbiAoc2VlIGV4YW1wbGUgYWJvdmUpLlxuICogQHBhcmFtIHtPYmplY3R9IGNvbmZpZyAtIENvbmZpZ3VyYXRpb24gb2JqZWN0IGZvciBgY2hlY2tvdXQuanNgLiBUaGUgb25seSBwYXJhbWV0ZXIgeW91XG4gKiBhYnNvbHV0ZWx5IGhhdmUgdG8gcGFzcyBpcyBga2V5YC4gVGhlIGBhbW91bnRgIGFuZCBgdG9rZW5gIHdpbGwgYmUgY3JlYXRlZCBhdXRvbWF0aWNhbGx5LlxuICogWW91IGNhbiB0aGVuIHBhc3MgYW55IG9wdGlvbmFsIHBhcmFtZXRlcnMgdG8gY3VzdG9taXplIHRoZSBjaGVja291dCBleHBlcmllbmNlLlxuXG4gKiBAcHJvcGVydHkge0hUTUxJbnB1dEVsZW1lbnR9IHRva2VuSW5wdXQgLSBBbiBpbnB1dCB3aXRoIGBpZD1cImlkX3N0cmlwZV90b2tlblwiYCB0byByZWNlaXZlXG4gKiB0aGUgZ2VuZXJhdGVkIHRva2VuLlxuICogQHByb3BlcnR5IHtIVE1MSW5wdXRFbGVtZW50W119IHRpY2tldElucHV0cyAtIE9uZSBvciBtb3JlIGlucHV0cyB3aXRoXG4gKiBgY2xhc3M9XCJ0aWNrZXQtcXVhbnRpdHlcImB3aXRoIGEgYGRhdGEtY29zdGAgYXR0cmlidXRlIHJlcHJlc2VudGluZyBpbmRpdmlkdWFsIHRpY2tldFxuICogcHJpY2VzIChpbiBjZW50cykuIFRoZSB2YWx1ZSBpcyB0aGUgYW1vdW50IG9mIHRpY2tldHMgdGhlIHVzZXIgd2lsbCBidXkuXG4gKiBAcHJvcGVydHkge0hUTUxJbnB1dEVsZW1lbnR9IFtkb25hdGlvbklucHV0XSAtIE9wdGlvbmFsIGlucHV0IHdpdGggYGlkPVwiaWRfZG9uYXRpb25cImBcbiAqIHRvIGluZGljYXRlIGEgZG9uYXRpb24gYW1vdW50ICh3aWxsIGJlIGNvbnZlcnRlZCB0byBjZW50cykuXG4gKlxuICogQHNlZSAge0BsaW5rIGh0dHBzOi8vc3RyaXBlLmNvbS9kb2NzL2NoZWNrb3V0I2ludGVncmF0aW9uLWN1c3RvbXxjaGVja291dC5qcyBkb2NzfVxuICovXG5leHBvcnQgZnVuY3Rpb24gcGF5V2l0aFN0cmlwZShmb3JtLCBjb25maWcpIHtcblx0Y29uc3QgdG9rZW5JbnB1dCA9IGZvcm0ucXVlcnlTZWxlY3RvcignI2lkX3N0cmlwZV90b2tlbicpO1xuXHRjb25zdCBkb25hdGlvbklucHV0ID0gZm9ybS5xdWVyeVNlbGVjdG9yKCcjaWRfZG9uYXRpb24nKTtcblx0Y29uc3QgdGlja2V0SW5wdXRzID0gZm9ybS5xdWVyeVNlbGVjdG9yQWxsKCcudGlja2V0LXF1YW50aXR5Jyk7XG5cblx0bGV0IHRvdGFsID0gMDsgLy8gVGhlIHRvdGFsIG11c3QgYmUgcGFzc2VkIGFzIGNlbnRzIHRvIFN0cmlwZVxuXHRsZXQgZG9uYXRpb24gPSAwO1xuXHRsZXQgcGF5bWVudENvbXBsZXRlID0gZmFsc2U7XG5cblx0Ly8gV2UgY2FuJ3QgZG8gYW55dGhpbmcgd2l0aG91dCBTdHJpcGVDaGVja291dFxuXHRpZiAodHlwZW9mIFN0cmlwZUNoZWNrb3V0ID09PSAndW5kZWZpbmVkJykge1xuXHRcdGFsZXJ0KCdFcnJvcjogQ291bGQgbm90IGNvbm5lY3QgdG8gb3VyIHBheW1lbnQgcHJvY2Vzc29yLicpO1xuXHRcdHJldHVybjtcblx0fVxuXG5cdC8vIEluZGljYXRlIHRoZSBmb3JtIGlzIGJlaW5nIHByb2Nlc3NlZFxuXHRmb3JtLmNsYXNzTGlzdC5hZGQoJ3dvcmtpbmcnKTtcblxuXHQvLyBJZiB0aGUgdG9rZW4gaW5wdXQgaXMgYWxyZWFkeSBmaWxsZWQgdXAsIGp1c3Qgc3VibWl0IHRoZSBmb3JtXG5cdGlmICh0b2tlbklucHV0LnZhbHVlKSB7XG5cdFx0Zm9ybS5zdWJtaXQoKTtcblx0XHRyZXR1cm47XG5cdH1cblxuXHQvLyBFbHNlLCBjYWxjdWxhdGUgdGhlIHRvdGFsIGJhc2VkIG9uIHRpY2tldHMgYW5kIGRvbmF0aW9uXG5cdFsuLi50aWNrZXRJbnB1dHNdLmZvckVhY2godGlja2V0SW5wdXQgPT4ge1xuXHRcdHRvdGFsICs9ICh0aWNrZXRJbnB1dC5kYXRhc2V0LmNvc3QgKiB0aWNrZXRJbnB1dC52YWx1ZSk7XG5cdH0pO1xuXHRpZiAoZG9uYXRpb25JbnB1dCkge1xuXHRcdGRvbmF0aW9uID0gcGFyc2VGbG9hdChkb25hdGlvbklucHV0LnZhbHVlKS50b0ZpeGVkKDIpO1xuXHRcdGRvbmF0aW9uID0gTWF0aC5yb3VuZChkb25hdGlvbiAqIDEwMCk7IC8vIENvbnZlcnQgdG8gY2VudHNcblx0XHR0b3RhbCArPSBkb25hdGlvbjtcblx0fVxuXG5cdC8vIENvbmZpZ3VyZSBhbmQgZGlzcGxheSB0aGUgY2hlY2tvdXQgcG9wLXVwXG5cdFN0cmlwZUNoZWNrb3V0Lm9wZW4oe1xuXHRcdC4uLmNvbmZpZyxcblx0XHRhbW91bnQ6IHRvdGFsLFxuXG5cdFx0Ly8gRXhlY3V0ZWQgYWZ0ZXIgdGhlIHVzZXIgZmlsbHMgdGhlIHBheW1lbnQgZGV0YWlsc1xuXHRcdHRva2VuOiBmdW5jdGlvbiBvblRva2VuUmVhZHkodG9rZW4pIHtcblx0XHRcdHRva2VuSW5wdXQudmFsdWUgPSB0b2tlbi5pZDtcblx0XHRcdHBheW1lbnRDb21wbGV0ZSA9IHRydWU7XG5cdFx0XHRmb3JtLnN1Ym1pdCgpO1xuXHRcdH0sXG5cblx0XHQvLyBFeGVjdXRlZCB3aGVuZXZlciB0aGUgcGF5bWVudCBkaWFsb2cgaXMgY2xvc2VkXG5cdFx0Ly8gZWl0aGVyIGFmdGVyIHRva2VuKCkgaXMgY2FsbGVkLCBvciB0aGUgdXNlciBjbG9zZXMgaXRcblx0XHRjbG9zZWQ6IGZ1bmN0aW9uIGNsZWFuVXAoKSB7XG5cdFx0XHQvLyBJZiB0aGUgdXNlciBjbG9zZWQgdGhlIHBvcC11cC4uLlxuXHRcdFx0aWYgKCFwYXltZW50Q29tcGxldGUpIHtcblx0XHRcdFx0Zm9ybS5jbGFzc0xpc3QucmVtb3ZlKCd3b3JraW5nJyk7XG5cdFx0XHR9XG5cdFx0fSxcblx0fSk7XG59XG5cbi8qKlxuICogSW5pdGlhbGl6ZSBHb29nbGUgTWFwcyB3aXRoIGEgbWFya2VyIGluIHBsYWNlLlxuICpcbiAqICoqWU9VIE1VU1QgSU5DTFVERSBHT09HTEUnUyBNQVAgQVBJIEJFRk9SRSBDQUxMSU5HIFRISVMgRlVOQ1RJT04qKlxuICpcbiAqIGBgYGh0bWxcbiAqIDxzY3JpcHQgc3JjPVwiaHR0cHM6Ly9tYXBzLmdvb2dsZWFwaXMuY29tL21hcHMvYXBpL2pzXCI+PC9zY3JpcHQ+XG4gKiA8c2NyaXB0PlxuICogICB2YXIgY2FudmFzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI21hcC1jYW52YXMnKTtcbiAqICAgdmFyIGFkZHJlc3MgPSAnMTYwMCBQZW5uc3lsdmFuaWEgQXZlbnVlLCBXYXNoaW5ndG9uIERDJztcbiAqICAgaW5pdE1hcChjYW52YXMsIGFkZHJlc3MpO1xuICogPC9zY3JpcHQ+XG4gKiBgYGBcbiAqXG4gKiBAcGFyYW0gIHtIVE1MRWxlbWVudH0gY2FudmFzICBUaGUgSFRNTCBlbGVtZW50IHRoYXQgd2lsbCByZWNlaXZlIHRoZSBtYXBcbiAqIEBwYXJhbSAge1N0cmluZ30gYWRkcmVzcyBUaGUgYWRkcmVzcyB3aGVyZSB0aGUgbWFwIG1hcmtlciB3aWxsIGJlIHBsYWNlZFxuICovXG5leHBvcnQgZnVuY3Rpb24gaW5pdE1hcChjYW52YXMsIGFkZHJlc3MpIHtcblx0Y29uc3QgZ2VvY29kZXIgPSBuZXcgZ29vZ2xlLm1hcHMuR2VvY29kZXIoKTtcblx0Z2VvY29kZXIuZ2VvY29kZSh7IGFkZHJlc3MgfSwgKHJlc3VsdHMsIHN0YXR1cykgPT4ge1xuXHRcdGlmIChzdGF0dXMgIT09IGdvb2dsZS5tYXBzLkdlb2NvZGVyU3RhdHVzLk9LKSB7XG5cdFx0XHRhbGVydChgR2VvY29kZSB3YXMgbm90IHN1Y2Nlc3NmdWwgZm9yIHRoZSBmb2xsb3dpbmcgcmVhc29uOiAke3N0YXR1c31gKTtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cdFx0Y29uc3QgbWFwID0gbmV3IGdvb2dsZS5tYXBzLk1hcChjYW52YXMsIHtcblx0XHRcdHpvb206IDE2LFxuXHRcdFx0Y2VudGVyOiByZXN1bHRzWzBdLmdlb21ldHJ5LmxvY2F0aW9uLFxuXHRcdH0pO1xuXHRcdGNvbnN0IG1hcmtlciA9IG5ldyBnb29nbGUubWFwcy5NYXJrZXIoe1xuXHRcdFx0bWFwLFxuXHRcdFx0cG9zaXRpb246IHJlc3VsdHNbMF0uZ2VvbWV0cnkubG9jYXRpb24sXG5cdFx0fSk7XG5cdFx0Y29uc3QgaW5mb1dpbmRvdyA9IG5ldyBnb29nbGUubWFwcy5JbmZvV2luZG93KHtcblx0XHRcdGNvbnRlbnQ6IGFkZHJlc3MsXG5cdFx0fSk7XG5cdFx0Z29vZ2xlLm1hcHMuZXZlbnQuYWRkTGlzdGVuZXIobWFya2VyLCAnY2xpY2snLCAoKSA9PiB7XG5cdFx0XHRpbmZvV2luZG93Lm9wZW4obWFwLCBtYXJrZXIpO1xuXHRcdH0pO1xuXHR9KTtcbn1cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9pbmRleC5qcyJdLCJzb3VyY2VSb290IjoiIn0=