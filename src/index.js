/* globals StripeCheckout google */
/* eslint-disable no-alert */

/**
 * Gathers payment info using Stripe's `checkout.js` and submits the generated token to the
 * server via a form. The `working` class will be added to the form while the pop-up is open.
 *
 * **YOU MUST INCLUDE `checkout.js` BEFORE CALLING THIS FUNCTION**
 *
 * For this DOM structure:
 *
 * ```html
 * <form id="my-form" method="POST">
 *  <input type="hidden" id="id_stripe_token" />
 *  <input type="number" id="id_donation" value="5" />
 *
 *  <input type="number" class="ticket-quantity" data-cost="100" value="3" />
 *  <input type="number" class="ticket-quantity" data-cost="200" value="2" />
 * </form>
 *
 * <script src="https://checkout.stripe.com/checkout.js"></script>
 * <script>
 *  payWithStripe(document.querySelector('#my-form'), {
 *  	key: 'YOUR KEY',
 *  	name: 'YOUR SITE NAME',
 *  	// Pass more checkout.js options here
 *  });
 * </script>
 * ```
 *
 * The user will be shown the pop-up with a charge of:
 *
 * - Donation: 5 (will be converted to 500 cents)
 * - Ticket A: 3 tickets, 100 each (already in cents)
 * - Ticket B: 2 tickets, 200 each (already in cents)
 * - Total in cents: 500 + 300 + 400 = 1200
 *
 * @param {HTMLFormElement} form - Will be submitted after payment details are captured. This
 * form must also contain all inputs listed in the properties section (see example above).
 * @param {Object} config - Configuration object for `checkout.js`. The only parameter you
 * absolutely have to pass is `key`. The `amount` and `token` will be created automatically.
 * You can then pass any optional parameters to customize the checkout experience.

 * @property {HTMLInputElement} tokenInput - An input with `id="id_stripe_token"` to receive
 * the generated token.
 * @property {HTMLInputElement[]} ticketInputs - One or more inputs with
 * `class="ticket-quantity"`with a `data-cost` attribute representing individual ticket
 * prices (in cents). The value is the amount of tickets the user will buy.
 * @property {HTMLInputElement} [donationInput] - Optional input with `id="id_donation"`
 * to indicate a donation amount (will be converted to cents).
 *
 * @see  {@link https://stripe.com/docs/checkout#integration-custom|checkout.js docs}
 */
export function payWithStripe(form, config) {
	const tokenInput = form.querySelector('#id_stripe_token');
	const donationInput = form.querySelector('#id_donation');
	const ticketInputs = form.querySelectorAll('.ticket-quantity');

	let total = 0; // The total must be passed as cents to Stripe
	let donation = 0;
	let paymentComplete = false;

	// We can't do anything without StripeCheckout
	if (typeof StripeCheckout === 'undefined') {
		alert('Error: Could not connect to our payment processor.');
		return;
	}

	// Indicate the form is being processed
	form.classList.add('working');

	// If the token input is already filled up, just submit the form
	if (tokenInput.value) {
		form.submit();
		return;
	}

	// Else, calculate the total based on tickets and donation
	[...ticketInputs].forEach(ticketInput => {
		total += (ticketInput.dataset.cost * ticketInput.value);
	});
	if (donationInput) {
		donation = parseFloat(donationInput.value).toFixed(2);
		donation = Math.round(donation * 100); // Convert to cents
		total += donation;
	}

	// Configure and display the checkout pop-up
	StripeCheckout.open({
		...config,
		amount: total,

		// Executed after the user fills the payment details
		token: function onTokenReady(token) {
			tokenInput.value = token.id;
			paymentComplete = true;
			form.submit();
		},

		// Executed whenever the payment dialog is closed
		// either after token() is called, or the user closes it
		closed: function cleanUp() {
			// If the user closed the pop-up...
			if (!paymentComplete) {
				form.classList.remove('working');
			}
		},
	});
}

/**
 * Initialize Google Maps with a marker in place.
 *
 * **YOU MUST INCLUDE GOOGLE'S MAP API BEFORE CALLING THIS FUNCTION**
 *
 * ```html
 * <script src="https://maps.googleapis.com/maps/api/js"></script>
 * <script>
 *   var canvas = document.querySelector('#map-canvas');
 *   var address = '1600 Pennsylvania Avenue, Washington DC';
 *   initMap(canvas, address);
 * </script>
 * ```
 *
 * @param  {HTMLElement} canvas  The HTML element that will receive the map
 * @param  {String} address The address where the map marker will be placed
 */
export function initMap(canvas, address) {
	const geocoder = new google.maps.Geocoder();
	geocoder.geocode({ address }, (results, status) => {
		if (status !== google.maps.GeocoderStatus.OK) {
			alert(`Geocode was not successful for the following reason: ${status}`);
			return;
		}
		const map = new google.maps.Map(canvas, {
			zoom: 16,
			center: results[0].geometry.location,
		});
		const marker = new google.maps.Marker({
			map,
			position: results[0].geometry.location,
		});
		const infoWindow = new google.maps.InfoWindow({
			content: address,
		});
		google.maps.event.addListener(marker, 'click', () => {
			infoWindow.open(map, marker);
		});
	});
}
